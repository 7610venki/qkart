package qkart;


import org.testng.annotations.*;


public class BaseTest extends BasePage{
    
    @BeforeClass
    public void driverSetup (){
       createDriver();
    }

    @AfterClass
    public void tearDown(){
        closeDriver();
    }


}
