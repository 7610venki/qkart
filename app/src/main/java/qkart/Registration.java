package qkart;

import java.sql.Timestamp;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class Registration extends BasePage{
     
     String URL = "https://crio-qkart-frontend-qa.vercel.app/register";
     String lastGeneratedusername;

    public void navigateToRegisterPage(){
        driver.get(URL);
    }

    public Boolean registerUser(String username, String password, Boolean dynamicUser){
 
      try {
        Timestamp ts = new Timestamp(System.currentTimeMillis());
      
        if(dynamicUser){
            username=username+" "+String.valueOf( ts.getTime()) ;
        }
        else {
            username = lastGeneratedusername;
        }
        
        WebElement uName = driver.findElement(By.id("username"));
        uName.click();
        uName.sendKeys(username);

        WebElement pWord = driver.findElement(By.name("password"));
        pWord.click();
        pWord.sendKeys(password);

        WebElement ConfirmpWord = driver.findElement(By.id("confirmPassword"));
        ConfirmpWord.click();
        ConfirmpWord.sendKeys(password);

        WebElement registerBtn = driver.findElement(By.xpath("//button[contains(text(),'Register Now')]"));
        registerBtn.click();

        waitTillVisibilityOfElement(driver.findElement(By.id("notistack-snackbar")));

        lastGeneratedusername = username;
       

        return driver.getCurrentUrl().contains("login");
      } catch (Exception e) {
         e.printStackTrace();
         System.out.println("user registration failed");
         return false;
      }
    }
}
