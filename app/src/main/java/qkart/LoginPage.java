package qkart;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class LoginPage extends BasePage {
    String URL ="https://crio-qkart-frontend-qa.vercel.app/login";

    Registration register = new Registration();

    public void naviogateToLoginPage(){
        driver.get(URL);
    }

    public boolean userLogin(String username , String password){
        boolean status = false;
        try {
        WebElement uName = driver.findElement(By.id("username"));
        uName.click();
        uName.sendKeys(username);

        WebElement pWord = driver.findElement(By.name("password"));
        pWord.click();
        pWord.sendKeys(password);

        WebElement loginButton = driver.findElement(By.xpath("//button[contains(text(),'Login to QKart')]"));
        loginButton.click();

        WebElement notifBar = driver.findElement(By.id("notistack-snackbar"));
        waitTillVisibilityOfElement(notifBar);

        String notifText = notifBar.getText();

        if( notifText.equalsIgnoreCase("Logged in successfully")){ 
            waitTillURLToBe("https://crio-qkart-frontend-qa.vercel.app/");
            String loggedinUser = driver.findElement(By.xpath("//p[contains(@class,'username')]")).getText();
            status = loggedinUser.equalsIgnoreCase(username);
            return status;
        }else{
            return status;}
        
        } catch (Exception e) {
           e.printStackTrace();
           return false;
        }
    }
}
